#ifndef SUPERSTUPIDCLASS_H
#define SUPERSTUPIDCLASS_H

#include <iostream>

class SuperStupidClass
{
public:
    SuperStupidClass();

    void sayHi() const;
    void setSuperStupidVar(const uint32_t &val);
    uint32_t getSuperStupidVal() const;

private:
    uint32_t m_superStupidVar;
};

#endif // SUPERSTUPIDCLASS_H
