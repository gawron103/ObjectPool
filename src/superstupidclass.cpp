#include "superstupidclass.h"

static uint32_t really_stupid_counter = 1u;

SuperStupidClass::SuperStupidClass() :
    m_superStupidVar(really_stupid_counter)
{
    ++really_stupid_counter;
}

void SuperStupidClass::sayHi() const
{
    std::cout << "Hi" << std::endl;
}

void SuperStupidClass::setSuperStupidVar(const uint32_t &val)
{
    m_superStupidVar = val;
}

uint32_t SuperStupidClass::getSuperStupidVal() const
{
    return m_superStupidVar;
}
