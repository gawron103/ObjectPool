#ifndef OBJECTPOOL_H
#define OBJECTPOOL_H

#include <iostream>
#include <memory>
#include <stack>

/**
 * @brief OBJECTS_AMOUNT: Amount of objects, which pool should have.
 * By default pool holds 10 objects, change var value to get more
 * objects.
 */
const static uint32_t OBJECTS_AMOUNT = 10u;

/**
 * @brief The ObjectPool class:
 * Class responsible for renting objects, based on automatic return of objects.
 */

template <class T>
class ObjectPool
{
private:
    /**
     * @brief m_data: Container responsible for holding available objects.
     */
    std::stack<T> m_data;

    /**
     * @brief The CDeleter struct:
     * Struct responsible for handling custom deleter of renting objects.
     * When pointer to the object is out of scope, it should not be deleted
     * (objects are on stack, they're are not dynamiclly created),
     * but be returned to the pool's container.
     */
    struct Deleter
    {
    public:
        Deleter(std::shared_ptr<std::stack<T> > data) :
            m_data(data)
        {

        }

        /**
         * @brief operator (): Deleter, which provides the return
         * of the object to the pool.
         * @param ptr: Pointer to the object.
         */
        void operator()(T* ptr)
        {
            m_data->push(*ptr);
        }

    private:
        /**
         * @brief m_data: Pointer to the pool's container.
         */
        std::shared_ptr<std::stack<T> > m_data = nullptr;
    };

public:
    /**
     * @brief ObjectPool: Constructor responsible for creating and adding
     * objects to stack. Number of added objects depends on OBJECTS_AMOUNT var.
     */
    ObjectPool()
    {
        while (m_data.size() < OBJECTS_AMOUNT)
        {
            m_data.push(T());
        }
    }

    /**
     * @brief rentObject: Method responsible for creating unique_ptr based on
     * object, which current is on the top of the stack.
     * @return unique_ptr which is an owner to the wanted object.
     */
    std::unique_ptr<T, Deleter> rentObject()
    {
        if (m_data.empty())
        {
            const std::string ERROR_MSG = "There are no available objects";
            throw ERROR_MSG;
        }

        std::shared_ptr<std::stack<T> > tempData(&m_data, [](std::stack<T> *container)
        {
            // There is a need to have a custom deleter, because when program is returning
            // from function, default deleter is launched, which cause malloc_error_break
            // (pointer being freed was not allocated). This destructor is should do
            // nothing. Pointer is casted only to mute compiler warning.

            static_cast<void>(container);
        });

        std::unique_ptr<T, Deleter> tempObj(&m_data.top(), tempData);

        m_data.pop();

        return tempObj;
    }

/**
     * @brief printData: Test method to check which objects the stack has in itself.
     */
void printData()
{
    while (!m_data.empty())
    {
        std::cout << m_data.top().getSuperStupidVal() << std::endl;
        m_data.pop();
    }
}

/**
     * @brief printObjectsCount: Test method to check how many objects are in the stack.
     */
uint32_t printObjectsCount() const
{
    return m_data.size();
}
};

#endif // OBJECTPOOL_H
