#ifndef TST_CHECKSIZE_H
#define TST_CHECKSIZE_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "objectpool.h"
#include "superstupidclass.cpp"

using namespace testing;

TEST(CheckSize1, NormalScenario)
{
    const uint8_t CORRECT_SIZE = 5u;

    ObjectPool<SuperStupidClass> pool;

    auto ptr1 = pool.rentObject();
    auto ptr2 = pool.rentObject();
    auto ptr3 = pool.rentObject();
    auto ptr4 = pool.rentObject();
    auto ptr5 = pool.rentObject();

    EXPECT_EQ(pool.printObjectsCount(), CORRECT_SIZE);
}

TEST(CheckSize2, NormalScenario)
{
    const uint8_t CORRECT_SIZE = 10u;

    ObjectPool<SuperStupidClass> pool;

    {
        auto ptr1 = pool.rentObject();
        auto ptr2 = pool.rentObject();
        auto ptr3 = pool.rentObject();
        auto ptr4 = pool.rentObject();
        auto ptr5 = pool.rentObject();
    }

    EXPECT_EQ(pool.printObjectsCount(), CORRECT_SIZE);
}

TEST(CheckSize3, NormalScenario)
{
    try
    {
        ObjectPool<SuperStupidClass> pool;

        {
            auto ptr1 = pool.rentObject();
            auto ptr2 = pool.rentObject();
            auto ptr3 = pool.rentObject();
            auto ptr4 = pool.rentObject();
            auto ptr5 = pool.rentObject();
            auto ptr6 = pool.rentObject();
            auto ptr7 = pool.rentObject();
            auto ptr8 = pool.rentObject();
            auto ptr9 = pool.rentObject();
            auto ptr10 = pool.rentObject();
            auto ptr11 = pool.rentObject();
        }
    }
    catch (const std::string ERROR)
    {
        const std::string EXPECTED_MSG = "There are no available objects";

        EXPECT_EQ(ERROR, EXPECTED_MSG);
    }
}

#endif // TST_CHECKSIZE_H
